# Tic Tac Toe
Ruby project per [The Odin Project](https://www.theodinproject.com/courses/ruby-programming/lessons/oop?ref=lnav)

## Game synopsis
Two-player console game.
Each cell is identified by a number (1-9)
The prompt identifies which player is up (X or O)
Each player simply types the number for the cell.
Win condition: 3 X's or O's in a row, horizontally/vertically/diagonally
Game prompts for rematch at the end.
